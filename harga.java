package com.dara;

public class harga {

    int SuperiorTwin = 315000;
    int SuperiorDouble = 375000;
    int DeluxeDouble = 535000;
    int DeluxeTwin = 590000;
    int JuniorSuite = 745000;
    int PresidentSuite = 920000;

    public int getTotal1(){
        int total;
        total = SuperiorTwin*10/100;
        return SuperiorTwin - total;
    }

    public int getTotal2(){
        int total;
        total = SuperiorDouble*20/100;
        return SuperiorDouble - total;
    }
    public int getTotal3(){
        int total;
        total = DeluxeDouble*20/100;
        return DeluxeDouble - total;
    }
    public int getTotal4(){
        int total;
        total = DeluxeTwin*15/100;
        return DeluxeTwin - total;
    }
    public int getTotal5(){
        int total;
        total = JuniorSuite;
        return JuniorSuite;
    }

    public int getTotal6(){
        int total;
        total = PresidentSuite*5/100;
        return PresidentSuite - total;
    }

}
