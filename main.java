package com.dara;

import java.io.FileNotFoundException;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        harga hr = new harga();
        member[] mm = new member[10];
        mm[0] = new member(5253);
        Scanner input = new Scanner(System.in);
        System.out.println("Halo, Selamat Datang");
        System.out.println("1. Member");
        System.out.println("2. Tamu");
        System.out.println("3. Admin");
        try (FileWriter f = new FileWriter("struk.txt", true);
             BufferedWriter b = new BufferedWriter(f);
             PrintWriter p = new PrintWriter(b)){
            System.out.print("Masuk Sebagai : ");
            int masuk = input.nextInt();
            System.out.println(" ");
            if (masuk == 1){
                try {
                    System.out.println("==============Aplikasi Reservasi Hotel===============");
                    System.out.print("\tMasukkan ID Member : ");
                    int id = input.nextInt();
                    input.nextLine();
                    System.out.print("\tNama Pemesan       : ");
                    String nama = input.nextLine();
                    System.out.print("\tTanggal Check In   : ");
                    int tanggal = input.nextInt();
                    System.out.print("\tTanggal Check Out  : ");
                    int tanggal2 = input.nextInt();
                    System.out.print("\tNo Handphone       : ");
                    int nohp = input.nextInt();
                    System.out.println(" ");
                    for(int i = 0; i < 10; i++){
                        if (mm[i].getID() == id){
                            System.out.println("\t--------------Tipe Kamar---------------");
                            System.out.println("\t1. Superior Twin");
                            System.out.println("\t2. Superior Double");
                            System.out.println("\t3. Deluxe Double");
                            System.out.println("\t4. Deluxe Twin");
                            System.out.println("\t5. Junior Suite");
                            System.out.println("\t6. President Suite");
                            System.out.println("\t----------------------------------------");
                            System.out.print("\tPilih Tipe Kamar : ");
                            int pilih = input.nextInt();
                            System.out.println(" ");
                            if (pilih == 1){
                                System.out.println("\tYou Get Discount 10%");
                                System.out.println("\tTotal Harga   : " +hr.getTotal1());
                                p.println("---------------Struk------------------");
                                p.println("ID Member         :" + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : Superior Twin");
                                p.println("Total Harga       : " + hr.getTotal1());
                                p.println("--------------------------------------");
                            }else if (pilih == 2){
                                System.out.println("\tYou Get Discount 20%");
                                System.out.println("\tTotal Harga   : " +hr.getTotal2());
                                p.println("Struk");
                                p.println("ID Member         :" + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : Superior Double");
                                p.println("Total Harga       : " + hr.getTotal2());
                                p.println("--------------------------------------");
                            }else if (pilih == 3){
                                System.out.println("\tYou Get Discount 20%");
                                System.out.println("\tTotal Harga   : " +hr.getTotal3());
                                p.println("Struk");
                                p.println("ID Member         :" + + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : Deluxe Double");
                                p.println("Total Harga       : " + hr.getTotal3());
                                p.println("--------------------------------------");
                            }else if (pilih == 4){
                                System.out.println("\tYou Get Discount 15%");
                                System.out.println("\tTotal Harga   : " +hr.getTotal4());
                                p.println("Struk");
                                p.println("ID Member         :" + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : Deluxe Twin");
                                p.println("Total Harga       : " + hr.getTotal4());
                                p.println("--------------------------------------");
                            }else if (pilih == 5) {
                                System.out.println("Total Harga   : " + hr.getTotal5());
                                p.println("Struk");
                                p.println("ID Member         :" + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : Junior Suite");
                                p.println("Total Harga       : " + hr.getTotal5());
                                p.println("--------------------------------------");
                            }else if (pilih == 6){
                                System.out.println("\tYou Get Discount 5%");
                                System.out.println("\tTotal Harga   : " +hr.getTotal6());
                                p.println("Struk");
                                p.println("ID Member         :" + mm[i].getID());
                                p.println("Nama Pemesan      : " + nama);
                                p.println("Tanggal Check In  : " + tanggal);
                                p.println("Tanggal Check Out : " + tanggal2);
                                p.println("No Handphone      : " + nohp);
                                p.println("Type Kamar        : President Suite");
                                p.println("Total Harga       : " + hr.getTotal6());
                                p.println("--------------------------------------");
                            }else {
                                throw new Exception("INPUT DATA YANG SESUAI");
                            }
                            break;
                        }
                    }
                }catch (NullPointerException r ){
                    System.out.println("ID TIDAK DITEMUKAN");
                }
                System.out.println("======================================================");
            }else if (masuk == 2){
                System.out.println("==============Aplikasi Reservasi Hotel===============");
                input.nextLine();
                System.out.print("\tNama Pemesan       : ");
                String nama = input.nextLine();
                System.out.print("\tTanggal Check In   : ");
                int tanggal = input.nextInt();
                System.out.print("\tTanggal Check Out  : ");
                int tanggal2 = input.nextInt();
                System.out.print("\tNo Handphone       : ");
                int nohp = input.nextInt();

                System.out.println(" ");
                System.out.println("\t---------Tipe Kamar----------");
                System.out.println("\t1. Superior Twin");
                System.out.println("\t2. Superior Double");
                System.out.println("\t3. Deluxe Double");
                System.out.println("\t4. Deluxe Twin");
                System.out.println("\t5. Junior Suite");
                System.out.println("\t6. President Suite");
                System.out.println("\t------------------------------");
                System.out.print("\tPilih Tipe Kamar : ");
                int pilih = input.nextInt();
                System.out.println(" ");
                if (pilih == 1){
                    System.out.println("\tYou Get Discount 10%");
                    System.out.println("\tTotal Harga   : " +hr.getTotal1());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : Superior Twin");
                    p.println("Total Harga       : " + hr.getTotal1());
                    p.println("--------------------------------------");
                }else if (pilih == 2){
                    System.out.println("\tYou Get Discount 20%");
                    System.out.println("\tTotal Harga   : " +hr.getTotal2());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : Superior Double");
                    p.println("Total Harga       : " + hr.getTotal2());
                    p.println("--------------------------------------");
                }else if (pilih == 3){
                    System.out.println("\tYou Get Discount 20%");
                    System.out.println("\tTotal Harga   : " +hr.getTotal3());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : Deluxe Twin");
                    p.println("Total Harga       : " + hr.getTotal3());
                    p.println("--------------------------------------");
                }else if (pilih == 4){
                    System.out.println("\tYou Get Discount 15%");
                    System.out.println("\tTotal Harga   : " +hr.getTotal4());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : Deluxe Double");
                    p.println("Total Harga       : " + hr.getTotal4());
                    p.println("--------------------------------------");
                }else if (pilih == 5) {
                    System.out.println("\tTotal Harga   : " + hr.getTotal5());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : Junior Suite");
                    p.println("Total Harga       : " + hr.getTotal5());
                    p.println("--------------------------------------");
                }else if (pilih == 6){
                    System.out.println("\tYou Get Discount 5%");
                    System.out.println("\tTotal Harga   : " +hr.getTotal6());
                    p.println("Struk");
                    p.println("Nama Pemesan      : " + nama);
                    p.println("Tanggal Check In  : " + tanggal);
                    p.println("Tanggal Check Out : " + tanggal2);
                    p.println("No Handphone      : " + nohp);
                    p.println("Type Kamar        : President Suite");
                    p.println("Total Harga       : " + hr.getTotal6());
                    p.println("--------------------------------------");
                }else {
                    throw new Exception("INPUT DATA YANG SESUAI");
                }

                System.out.println("======================================================");
            }else if (masuk == 3){
                System.out.print("Masukkan ID Admin : ");
                int a = input.nextInt();
                if (a == 1234){
                    System.out.println("========DATA TRANSAKSI HOTEL=========");
                    String fileName = "struk.txt";
                     try {
                         File myFile = new File(fileName);
                         Scanner fileReader = new Scanner(myFile);
                         while (fileReader.hasNextLine()){
                             String data = fileReader.nextLine();
                             System.out.println(data);
                         }
                     }catch (FileNotFoundException e) {
                         System.out.println("Terjadi Kesalahan: " + e.getMessage());
                         e.printStackTrace();
                     }
                }else {
                    throw new Exception("ID Admin Tidak Ditemukan");
                }
            }else {
                throw new Exception("INPUT DATA TIDAK SESUAI");
            }

        }catch (Exception z){
            z.printStackTrace();
        }
    }
}
